import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import curve_fit

# 假设已有 x, y, z 三个列表
# 这里使用示例数据，你需要替换为实际数据
x = []
y = []
z = []
with open('./0.001-0.01.txt', 'r') as file:
    for line in file:
        parts = line.split()
        test_recall = float(parts[3][:-1])
        z.append(test_recall)
for a in range(10):
    for b in range(10):
        x.append((a+1)*0.001)
        y.append((b+1)*0.001)

# 定义曲面拟合的函数，这里使用二次多项式
def func(X, a, b, c):
    x, y = X
    return a * x**2 + b * y**2 + c

# 转换列表为 NumPy 数组
x_data = np.array(x)
y_data = np.array(y)
z_data = np.array(z)

# 使用curve_fit进行曲面拟合
params, covariance = curve_fit(func, (x_data, y_data), z_data)

# 创建网格数据进行曲面绘制
x_range = np.linspace(min(x_data), max(x_data), 100)
y_range = np.linspace(min(y_data), max(y_data), 100)
X, Y = np.meshgrid(x_range, y_range)
Z = func((X, Y), *params)

# 绘制三维散点图和拟合曲面
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x_data, y_data, z_data, c='r', marker='o', label='Sample Points')
ax.plot_surface(X, Y, Z, alpha=0.5, rstride=100, cstride=100, color='b', label='Fitted Surface')

# 设置图形标签
ax.set_xlabel('G_lr')
ax.set_ylabel('D_lr')
ax.set_zlabel('Recall')
#ax.legend()

plt.show()
