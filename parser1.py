import argparse

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset',nargs='?',default="tiktok",help="数据集")
    parser.add_argument('--data_path',default='./data/',help="数据集位置")
    parser.add_argument('--keep_rate',type=float,default=0.8)
    parser.add_argument('--emb_size',type=int,default=128)
    parser.add_argument('--gpu_id',type=int, default=0, help='GPU id')
    parser.add_argument('--G_lr',type=float,default=0.001)
    parser.add_argument('--D_lr',type=float,default=0.001)
    parser.add_argument('--lr', type=float, default=0.0006)
    parser.add_argument('--batch_size',type=int,default=256)
    parser.add_argument('--epoch',type=int,default=1000)
    parser.add_argument('--head_num', type=int, default=4)
    parser.add_argument('--Ks', nargs='?', default='[10, 20, 50]', help='K value of ndcg/recall @ k')
    parser.add_argument('--early_stop', type=int, default=7, help='')
    parser.add_argument('--Recall_rate', type=float, default=0.015, help='')
    parser.add_argument('--decay', type=float, default=1e-5)
    return parser.parse_args()

args = parse_args()